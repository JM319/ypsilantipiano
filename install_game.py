import os
import shutil
import sys

class Copier:
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest

    def copy(self, src, dest):
        shutil.copy(os.path.join(self.src, src),
                    os.path.join(self.dest, dest))

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: {0} <toybox_home>'.format(sys.argv[0]))
        sys.exit(1)

    source_dir = sys.argv[1]
    if not os.path.isdir(source_dir):
        print('{0} is not a directory'.format(source_dir))

    copier = Copier(source_dir, os.path.join('app', 'static'))
    copier.copy('toybox.js', 'js')
    copier.copy('toybox.wasm', 'js')
    copier.copy('toybox.wasm.map', 'js')
    copier.copy('toybox.wast', 'js')
    copier.copy('toybox.data', 'html')

    # TODO: Reroute toybox.js import
    # copier.copy('toybox.html', 'html')
