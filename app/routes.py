from app import app
from flask import render_template


@app.route('/')
def root():
    return app.send_static_file('html/index.html')


@app.route('/goal')
def goal_cards():
    attributes = [{'name': 'articulation'},
                  {'name': 'dynamics'},
                  {'name': 'rhythm'}]
    return render_template('card.html', attributes=attributes)


@app.route('/toybox')
def toybox():
    return app.send_static_file('html/toybox.html')


@app.route('/toybox.data')
def data():
    return app.send_static_file('html/toybox.data')
