const sections: Array<string> = ["curriculum", "about", "faq", "policies"];

function getTab(sectionName:string):JQuery {
    return $("#" + sectionName + "-tab");
}

function getContent(sectionName:string):JQuery {
    return $("#" + sectionName + "-content");

}

function hideSections():void {
    for (let section of sections) {
        getContent(section).hide();
        getTab(section).children()[0].style.color = "white";
    }
}

function showSection(sectionName:string) {
    hideSections();
    getContent(sectionName).show();
    console.log(getTab(sectionName).children().toString());
    getTab(sectionName).children()[0].style.color = "gold";
}

function setTabListeners():void {
    for (let section of sections) {
        getTab(section).click(function() {
            showSection(section);
        });
    }
}

$(document).ready(function() {
    setTabListeners();
    showSection("about");
});