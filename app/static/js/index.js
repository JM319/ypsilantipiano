var sections = ["curriculum", "about", "faq", "policies"];
function getTab(sectionName) {
    return $("#" + sectionName + "-tab");
}
function getContent(sectionName) {
    return $("#" + sectionName + "-content");
}
function hideSections() {
    for (var _i = 0, sections_1 = sections; _i < sections_1.length; _i++) {
        var section = sections_1[_i];
        getContent(section).hide();
        getTab(section).children()[0].style.color = "white";
    }
}
function showSection(sectionName) {
    hideSections();
    getContent(sectionName).show();
    console.log(getTab(sectionName).children().toString());
    getTab(sectionName).children()[0].style.color = "gold";
}
function setTabListeners() {
    var _loop_1 = function(section) {
        getTab(section).click(function () {
            showSection(section);
        });
    };
    for (var _i = 0, sections_2 = sections; _i < sections_2.length; _i++) {
        var section = sections_2[_i];
        _loop_1(section);
    }
}
$(document).ready(function () {
    setTabListeners();
    showSection("about");
});
//# sourceMappingURL=index.js.map